$(document).ready(function(){
    $('.intro-slick').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        dots: true,
        arrows: true,
        customPaging : function(slider, i) {
            var thumb = $(slider.$slides[i]).data('thumb');
            return '<a>'+thumb+'</a>';
        },
    });
    $('.equipments-slick').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        dots: false,
        arrows: true
    });
    $('.letter-slick').slick({
        slidesToShow: 5,
        slidesToScroll: 3,
        autoplay: false,
        dots: false,
        arrows: true
    });
    $('.reviews-slick').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        dots: true,
        arrows: true
    });
    $('.instagram-slick').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        dots: false,
        arrows: true
    });
    $('.instagram-body-slick').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        dots: false,
        arrows: true
    });
    $('.team-slick').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        dots: true,
        arrows: true
    });
    $('.works-slick').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: false,
        dots: false,
        arrows: true,
        rows: 3,
        infinite: false
    });
});
