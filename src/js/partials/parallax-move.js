window.onload = function () {

    // группы компаний
	var groupsParallax = document.getElementById ( 'groupsParallax' );
	var groupsHouseLeft = document.getElementById ( 'groupsHouse' ).offsetLeft,
	groupsHouseTop = document.getElementById ( 'groupsHouse' ).offsetTop,
	groupsCloudsLeft = document.getElementById ( 'groupsClouds' ).offsetLeft,
	groupsCloudsTop = document.getElementById ( 'groupsClouds' ).offsetTop;

	groupsParallax.onmousemove = function ( event ) {
		event = event || window.event;
		var x = event.clientX - groupsParallax.offsetLeft,
		y = event.clientY - groupsParallax.offsetTop;

		mouseParallax ( 'groupsHouse', groupsHouseLeft, groupsHouseTop, x, y, -20 );
		mouseParallax ( 'groupsClouds', groupsCloudsLeft, groupsCloudsTop, x, y, 20 );
	}


    // интро экстерьер
    var exteriorParallax = document.getElementById ( 'exteriorParallax' );
    var exteriorCloudsLeft = document.getElementById ( 'exteriorClouds' ).offsetLeft,
    exteriorCloudsTop = document.getElementById ( 'exteriorClouds' ).offsetTop,
    exteriorHandLeft = document.getElementById ( 'exteriorHand' ).offsetLeft,
    exteriorHandTop = document.getElementById ( 'exteriorHand' ).offsetTop;

    exteriorParallax.onmousemove = function ( event ) {
        event = event || window.event;
        var x = event.clientX - exteriorParallax.offsetLeft,
        y = event.clientY - exteriorParallax.offsetTop;

        mouseParallax ( 'exteriorClouds', exteriorCloudsLeft, exteriorCloudsTop, x, y, 30 );
        mouseParallax ( 'exteriorHand', exteriorHandLeft, exteriorHandTop, x, y, 10 );
    }


    // интро интерьер
    var interiorParallax = document.getElementById ( 'interiorParallax' );
    var interiorHandLeft = document.getElementById ( 'interiorHand' ).offsetLeft,
    interiorHandTop = document.getElementById ( 'interiorHand' ).offsetTop;

    interiorParallax.onmousemove = function ( event ) {
        event = event || window.event;
        var x = event.clientX - interiorParallax.offsetLeft,
        y = event.clientY - interiorParallax.offsetTop;

        mouseParallax ( 'interiorHand', interiorHandLeft, interiorHandTop, x, y, 10 );
    }

}

function mouseParallax ( id, left, top, mouseX, mouseY, speed ) {
	var obj = document.getElementById ( id );
	var parentObj = obj.parentNode,
	containerWidth = parseInt( parentObj.offsetWidth ),
	containerHeight = parseInt( parentObj.offsetHeight );
	obj.style.left = left - ( ( ( mouseX - ( parseInt( obj.offsetWidth ) / 2 + left ) ) / containerWidth ) * speed ) + 'px';
	obj.style.top = top - ( ( ( mouseY - ( parseInt( obj.offsetHeight ) / 2 + top ) ) / containerHeight ) * speed ) + 'px';
}
